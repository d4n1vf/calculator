Feature: Multiplication

   So I can make a multiplication
   I must inform two numbers

    Scenario: Multiplying numbers successfully
        Given I have two numbers: "10" and "5"
        When I multiply them
        Then I get the message "Multiplication 10 by 5 is equal to 50."

    Scenario: Multiplier is not a number
        Given I have two numbers: "A10" and "5"
        When I multiply them
        Then I get the message "Multiplication only works between numbers!"

    Scenario: Multiplicand is not a number
        Given I have two numbers: "10" and "B5"
        When I multiply them
        Then I get the message "Multiplication only works between numbers!"