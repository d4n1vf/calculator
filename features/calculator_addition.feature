Feature: Addition
    JIRA-3456
    So I can make a addition
    I must inform two numbers

    Scenario: Adding numbers successfully
        Given I have two numbers: "10" and "5"
        When I add them
        Then I get the message "Addition 10 by 5 is equal to 15."

    Scenario: Firts addend is not a number
        Given I have two numbers: "A10" and "5"
        When I add them
        Then I get the message "Addition only works between numbers!"

    Scenario: Second addend is not a number
        Given I have two numbers: "10" and "B5"
        When I add them
        Then I get the message "Addition only works between numbers!"




        