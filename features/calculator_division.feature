Feature: Division

   So I can make a division
   I must inform two numbers
   Since the divisor must be nonzero

    Scenario: Dividing numbers successfully
        Given I have two numbers: "10" and "5"
        When I divide them
        Then I get the message "Division 10 by 5 is equal to 2."

    Scenario: Dividend is not a number
        Given I have two numbers: "A10" and "5"
        When I divide them
        Then I get the message "Division only works between numbers!"

    Scenario: Divisor is not a number
        Given I have two numbers: "10" and "B5"
        When I divide them
        Then I get the message "Division only works between numbers!"

    Scenario: Divisor is zero
        Given I have two numbers: "10" and "0"
        When I divide them
        Then I get the message "Divisor can not be zero!"