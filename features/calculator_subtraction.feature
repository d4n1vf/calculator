Feature: Subtraction

   So I can make a subtraction
   I must inform two numbers

    Scenario: Subtracting numbers successfully
        Given I have two numbers: "10" and "5"
        When I subtract them
        Then I get the message "Subtraction 10 by 5 is equal to 5."

    Scenario: Minuend is not a number
        Given I have two numbers: "A10" and "5"
        When I subtract them
        Then I get the message "Subtraction only works between numbers!"

    Scenario: Subtrahend is not a number
        Given I have two numbers: "10" and "B5"
        When I subtract them
        Then I get the message "Subtraction only works between numbers!"