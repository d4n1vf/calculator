require_relative '../../app/calculator'

Given("I have two numbers: {string} and {string}") do |numero_1, numero_2|
  @n1 = numero_1
  @n2 = numero_2
end

When("I divide them") do
  @calc = Calculator.new
  @calc.divide(@n1, @n2)
end

When("I add them") do
  @calc = Calculator.new
  @calc.add(@n1, @n2)
end

When("I subtract them") do
  @calc = Calculator.new
  @calc.subtract(@n1, @n2)
end

When("I multiply them") do
  @calc = Calculator.new
  @calc.multiply(@n1, @n2)
end

Then("I get the message {string}") do |message|
  expect(@calc.message).to eql message
end
