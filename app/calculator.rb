class Calculator
  attr_accessor :result, :message

  def is_number?(n)
    n.to_f.to_s == n.to_s || n.to_i.to_s == n.to_s
  end

  def add(n1, n2)
    num_1 = n1.to_i
    num_2 = n2.to_i
    if !is_number?(n1) || !is_number?(n2)
      self.message = 'Addition only works between numbers!'
    else
      self.result = num_1 + num_2
      self.message = "Addition #{n1} by #{n2} is equal to #{self.result}."
    end
  end

  def subtract(n1, n2)
    num_1 = n1.to_i
    num_2 = n2.to_i
    if !is_number?(n1) || !is_number?(n2)
      self.message = 'Subtraction only works between numbers!'
    else
      self.result = num_1 - num_2
      self.message = "Subtraction #{n1} by #{n2} is equal to #{self.result}."
    end
  end

  def multiply(n1, n2)
    num_1 = n1.to_i
    num_2 = n2.to_i
    if !is_number?(n1) || !is_number?(n2)
      self.message = 'Multiplication only works between numbers!'
    else
      self.result = num_1 * num_2
      self.message = "Multiplication #{n1} by #{n2} is equal to #{self.result}."
    end
  end

  def divide(n1, n2)
    num_1 = n1.to_i
    num_2 = n2.to_i
    if !is_number?(n1) || !is_number?(n2)
      self.message = 'Division only works between numbers!'
    elsif num_2 == 0
      self.message = 'Divisor can not be zero!'
    else
      self.result = num_1 / num_2
      self.message = "Division #{n1} by #{n2} is equal to #{self.result}."
    end
  end
end
